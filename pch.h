#pragma once

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)  

#ifndef _DEBUG
#define NDEBUG 1
#endif

// fuck
#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING

#define NO_COPY_NO_MOVE_CLASS_DEF(class_name) \
	class_name(const class_name&) = delete;\
	class_name(class_name&&) = delete;\
	class_name& operator =(const class_name&) = delete;\
	class_name& operator =(class_name&&) = delete;

#define GLM_FORCE_CTOR_INIT

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/fast_trigonometry.hpp>
#include <glm/gtx/intersect.hpp>


using f32 = float;
using f64 = double;

using u8 = uint8_t;
using i8 = int8_t;

using u16 = uint16_t;
using i16 = int16_t;

using u32 = uint32_t;
using i32 = int32_t;

using u64 = uint64_t;
using i64 = int64_t;

using glm::vec4;
using glm::vec3;
using glm::vec2;
using glm::mat3;
using glm::mat4;
using glm::quat;

using glm::ivec2;
using glm::ivec3;
using glm::ivec4;


#include <iostream>
#include <vector>
#include <unordered_map>
#include <functional>
#include <optional>
#include <thread>
#include <mutex>
#include <filesystem>
#include <variant>
#include <any>
#include <functional>
#include <random>
#include <array>

namespace fs = std::filesystem;
using namespace std;

const inline string res_folder_path = "./../res/"s;

namespace std
{
	template<>
	struct std::hash<ivec2>
	{
		std::size_t operator()(ivec2 const& s) const noexcept
		{
			return std::hash<int>()(s.x) | std::hash<int>()(s.x) * 3476;
		}
	};
}
