#pragma once

#include <string>

using namespace std;

void AssertFailureImpl(const char* file, int line, const char* function, const std::string& msg);
#define Assert(ex)  (void)(                                                   \
            (!!(ex)) ||                                                       \
            (AssertFailureImpl(__FILENAME__, __LINE__, __FUNCTION__, #ex), 0) \
        )
#define AssertFail()  (void)(                                                            \
                                                                              \
            (AssertFailureImpl(__FILENAME__, __LINE__, __FUNCTION__, "<no message>"), 0) \
        )
#define AssertMsg(ex, msg)  (void)(                                             \
            (!!(ex)) ||                                                         \
            (AssertFailureImpl(__FILENAME__, __LINE__, __FUNCTION__, (msg)), 0) \
        )
#define AssertMsgFail(msg)  (void)(                                             \
                                                                     \
            (AssertFailureImpl(__FILENAME__, __LINE__, __FUNCTION__, (msg)), 0) \
        )