#pragma once
#include "pch.h"
#include "Assert.h"

template <typename T>
class matrix
{
public:
	void resize(size_t size_x, size_t size_y);

	void clear();

	size_t size_x() const;
	size_t size_y() const;
	size_t size() const;

	auto begin();
	auto end();
	auto data();
	auto data() const;

	class matrix_index_proxy
	{
	public:
		T& operator [](size_t y);
	private:
		matrix_index_proxy(matrix<T>& m, size_t x);

		matrix<T>& _m;
		size_t _x;
		friend class matrix;
	};

	class matrix_index_proxy_const
	{
	public:
		const T& operator [](size_t y) const;
	private:
		matrix_index_proxy_const(const matrix<T>& m, size_t x);

		const matrix<T>& _m;
		size_t _x;
		friend class matrix;
	};

	matrix_index_proxy operator [](size_t x);
	matrix_index_proxy_const operator [](size_t x) const;

private:
	size_t _size_x = 0;
	vector<T> _data;
};

template <typename T>
void matrix<T>::resize(size_t size_x, size_t size_y)
{
	_data.clear();
	_data.resize(size_x * size_y);
	_size_x = size_x;
}

template <typename T>
void matrix<T>::clear()
{
	_data.clear();
	_size_x = 0;
}

template <typename T>
size_t matrix<T>::size_x() const
{
	return _size_x;
}

template <typename T>
size_t matrix<T>::size_y() const
{
	return _data.size() / _size_x;
}

template <typename T>
size_t matrix<T>::size() const
{
	return _data.size();
}

template <typename T>
auto matrix<T>::begin()
{
	return _data.begin();
}

template <typename T>
auto matrix<T>::end()
{
	return _data.end();
}

template <typename T>
auto matrix<T>::data()
{
	return _data.data();
}

template <typename T>
auto matrix<T>::data() const
{
	return _data.data();
}

template <typename T>
T& matrix<T>::matrix_index_proxy::operator[](size_t y)
{
	Assert(y < _m.size_y());
	return _m._data[y * _m._size_x + _x];
}

template <typename T>
matrix<T>::matrix_index_proxy::matrix_index_proxy(matrix<T>& m, size_t x) :
	_m(m),
	_x(x)
{
	Assert(x < _m._size_x);
}

template <typename T>
const T& matrix<T>::matrix_index_proxy_const::operator[](size_t y) const
{
	Assert(y < _m.size_y());
	return _m._data[y * _m._size_x + _x];
}

template <typename T>
matrix<T>::matrix_index_proxy_const::matrix_index_proxy_const(const matrix<T>& m, size_t x) :
	_m(m),
	_x(x)
{
	Assert(x < _m._size_x);
}

template <typename T>
typename matrix<T>::matrix_index_proxy matrix<T>::operator[](size_t x)
{
	return matrix_index_proxy(*this, x);
}

template <typename T>
typename matrix<T>::matrix_index_proxy_const matrix<T>::operator[](size_t x) const
{
	return matrix_index_proxy_const(*this, x);
}
