

#include <ft2build.h>
#include FT_FREETYPE_H
#pragma comment( lib, "freetype.lib" )

#include <stb/stb_image.h>
#include <stb/stb_image_write.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <pugixml/pugixml.hpp>
#ifndef _DEBUG
#pragma comment (lib, "pugixml.lib")
#else
#pragma comment (lib, "pugixmld.lib")
#endif
#include "TextureCpu.h"

#include <vector>
#include <algorithm>
#include <iostream>
#include <unordered_map>

using namespace std;

struct Glyph
{
	TextureCpu texture;
	ivec2 size;
	ivec2 offset;
	i32 advance;
};

unordered_map<uint64_t, Glyph> LoadGlyphs(fs::path const& font_path, int font_size, vec4 color)
{
	unordered_map<uint64_t, Glyph> glyphs;

	FT_Library ft;
	if (FT_Init_FreeType(&ft))
	{
		cout << "ERROR: init FreeType" << endl;
		terminate();
	}

	FT_Face face;
	if (FT_New_Face(ft, font_path.string().c_str(), 0, &face))
	{
		cout << "ERROR: load font" << endl;
		terminate();
	}

	FT_Set_Pixel_Sizes(face, 0, font_size);

	FT_UInt index;
	FT_ULong character = FT_Get_First_Char(face, &index);

	while (true)
	{
		// to do something


		if (FT_Load_Char(face, character, FT_LOAD_RENDER))
		{
			cout << "ERROR: load glyph" << endl;
			continue;
		}

		ivec2 glyph_size(face->glyph->bitmap.width, face->glyph->bitmap.rows);
		u8* data = face->glyph->bitmap.buffer;

		TextureCpu texture;
		auto& pixels = texture.DataRaw();
		pixels.resize(glyph_size.x, glyph_size.y);
		for (int x = 0; x < glyph_size.x; x++)
			for (int y = 0; y < glyph_size.y; y++)
			{
				auto& pixel = pixels[x][y];
				pixel.r = color.r * 255;
				pixel.g = color.g * 255;
				pixel.b = color.b * 255;
				pixel.a = color.a * data[x + y * glyph_size.x];
			}

		if (pixels.size() == 0)
		{
			pixels.resize(1, 1);
			pixels.begin()->r = color.r;
			pixels.begin()->g = color.g;
			pixels.begin()->b = color.b;
			pixels.begin()->a = 0;
		}

		Glyph glyph = {
			texture,
			ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
			ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
			lround(face->glyph->advance.x / 64.0f)
		};

		glyphs[character] = glyph;
		character = FT_Get_Next_Char(face, character, &index);
		if (!index) break;
	}

	FT_Done_Face(face);
	FT_Done_FreeType(ft);

	cout << "Glyphs load ok:" << glyphs.size() << endl;

	return glyphs;
}

void SaveGlyph(TextureCpu const& texture, fs::path const& path, string const& name)
{
	stbi_write_png((path.string() + name).c_str(), texture.Size().x, texture.Size().y, 4, texture.Data(), texture.Size().x * 4);
}

void SaveGlyphs(unordered_map<uint64_t, Glyph> const& glyphs, fs::path path, string const& prefix)
{
	pugi::xml_document doc;

	for (auto& pair : glyphs)
	{
		u64 code = pair.first;
		auto& glyph = pair.second;

		auto node = doc.append_child("Glyph");
		node.append_attribute("code") = code;
		node.append_attribute("size_x") = glyph.size.x;
		node.append_attribute("size_y") = glyph.size.y;
		node.append_attribute("offset_x") = glyph.offset.x;
		node.append_attribute("offset_y") = glyph.offset.y;
		node.append_attribute("advance") = glyph.advance;
		string glyph_filename = prefix + to_string(code) + ".png";
		SaveGlyph(glyph.texture, path, glyph_filename);
	}

	cout << "Save glyphs done" << endl;

	bool res = doc.save_file((path.string() + "glyph_index.xml").c_str(), "\t", pugi::format_no_declaration);

	AssertMsg(res, "Cant save xml");

	cout << "Save xml done" << endl;
}

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		if (argc == 0)
		{
			cout << "Unknown launch" << endl;
		}
		else
		{
			cout << "Usage: " << fs::path(argv[0]).filename() << " <in font path> <out folder> <out prefix> <font size>" << endl;
		}
		terminate();
	}

	fs::path folder_path = argv[2] + "\\"s;
	fs::path font_path = argv[1];
	string prefix(argv[3]);
	int font_size(atoi(argv[4]));

	auto glyphs = LoadGlyphs(font_path, font_size, vec4(1));

	SaveGlyphs(glyphs, folder_path, prefix);
	return 0;
}
