#pragma once

#include "pch.h"
#include "matrix.h"

struct TextureCpu
{
	struct Pixel
	{
		u8 r;
		u8 g;
		u8 b;
		u8 a;
	};

	void Load(const fs::path& path);
	void LoadChannels(const fs::path& path_r, const fs::path& path_g, const fs::path& path_b);
	Pixel& GetPixel(size_t x, size_t y);
	const Pixel& GetPixel(size_t x, size_t y) const;
	const Pixel* Data() const;
	auto& DataRaw() { return _data; }
	ivec2 Size() const;

private:
	matrix<Pixel> _data;
};
