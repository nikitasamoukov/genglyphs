#include "TextureCpu.h"

#include <stb/stb_image.h>

void TextureCpu::Load(const fs::path& path)
{
	int size_x = 0;
	int size_y = 0;
	unsigned char* image = stbi_load(path.string().c_str(), &size_x, &size_y, nullptr, STBI_rgb_alpha);
	AssertMsg(image, "Bad load image:" + path.string());

	_data.resize(size_x, size_y);
	auto data = _data.data();
	for (size_t i = 0; i < _data.size(); i++)
	{
		data[i] = {
			image[i * 4 + 0],
			image[i * 4 + 1],
			image[i * 4 + 2],
			image[i * 4 + 3]
		};
	}

	stbi_image_free(image);
}

void TextureCpu::LoadChannels(const fs::path& path_r, const fs::path& path_g, const fs::path& path_b)
{
	array<string, 3> tex_names = { path_r.string(), path_g.string(), path_b.string() };
	array<unsigned char*, 3> tex_data{};
	array<ivec2, 3> tex_size{};

	for (size_t i = 0; i < 3; i++)
	{
		tex_data[i] = stbi_load(tex_names[i].c_str(), &tex_size[i].x, &tex_size[i].y, nullptr, STBI_grey);
	}

	for (size_t i = 0; i < 3; i++)
	{
		AssertMsg(tex_data[i], "Bad load image:" + tex_names[i]);
		AssertMsg(tex_size[i] == tex_size[0], "Diff size textures:" +
			tex_names[i] + "(" + to_string(tex_size[i].x) + ", " + to_string(tex_size[i].y) + ")" +
			", " +
			tex_names[0] + "(" + to_string(tex_size[0].x) + ", " + to_string(tex_size[0].y) + ")");
	}

	_data.resize(tex_size[0].x, tex_size[0].y);
	auto data = _data.data();

	for (size_t i = 0; i < _data.size(); i++)
	{
		data[i] = {
			tex_data[0][i],
			tex_data[1][i],
			tex_data[2][i],
			255
		};
	}
	for (auto data_ptr : tex_data) stbi_image_free(data_ptr);
}

TextureCpu::Pixel& TextureCpu::GetPixel(size_t x, size_t y)
{
	return _data[x][y];
}

const TextureCpu::Pixel& TextureCpu::GetPixel(size_t x, size_t y) const
{
	return _data[x][y];
}

const TextureCpu::Pixel* TextureCpu::Data() const
{
	return _data.data();
}

ivec2 TextureCpu::Size() const
{
	return { _data.size_x(), _data.size_y() };
}
